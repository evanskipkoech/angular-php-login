angular.module('UserAuthTutorial', ['ui.router', 'UserAuthTutorial.loginController', 'UserAuthTutorial.mainController', 'UserAuthTutorial.authenticationService'])

	.config(function($stateProvider, $urlRouterProvider){

		$urlRouterProvider.otherwise('/');
		
		$stateProvider

		.state('login', {
			url: '/',
			controller : 'loginController',
			templateUrl: 'views/login.html'
		})

		.state('application',{
			url: '/app',
			controller: 'mainController',
			templateUrl: 'views/application.html'
		});

	});