angular.module('UserAuthTutorial.authenticationService', ['ui.router'])

	.service('authenticationService', ['$http','$state', function($http, $state){
		var self = this;
		self.checkToken = function (token) {

			var data = {token: token};

			console.log("Token to be Authenticated: "+token);

			$http.post("./php/checktoken/", data).then(function successCallback(response) {
					
					console.log("Auth Status " +response.data);
					
					if (response.data ==="unauthorized") {
						console.log("Logged out");
						$state.go("login");
					}else{
						return response;
					}

				}, function errorCallback(error) {
					console.log("Logged in");
					console.log(error);
				}
			);

		};
		
	}])
;