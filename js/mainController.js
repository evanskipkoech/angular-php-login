angular.module('UserAuthTutorial.mainController', ['ui.router'])
	
	.controller('mainController', function($scope, $state, $http, authenticationService){

		console.log(localStorage.token);
		
		var token = JSON.parse(localStorage.token || null);

		authenticationService.checkToken(token);

		$scope.logout = function() {
			var data ={
				token : token
			};

			console.log("Token passed to logout endpoint: "+token);

			$http.post('./php/logout/', data).then(function successCallback(response) {
				console.log("Log out post response: "+response.data);
				localStorage.clear();
				console.log("Cleared Token: "+localStorage.token);
				$state.go('login');
			}, function errorCallback(error) {
				console.log(error);
			});
		};
	}) 
;